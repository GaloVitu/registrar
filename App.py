#render_template: renderizar plantillas HTML en Flask
#request: acceso a la información de la solicitud del cliente
#redirect: redirigir a una URL diferente
#url_for: genera URLs para funciones de vista en tu aplicación Flask
#flash: mostrar mensajes temporales al usuario después de ciertas acciones
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL 

app = Flask(__name__)

#CONEXION
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'prueba'
mysql = MySQL(app)

#SETTINGS
app.secret_key = 'mysecretkey'

@app.route('/')
def Index():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM usuarios')
    data = cur.fetchall()
    return render_template('index.html', usuarios = data)

@app.route('/add', methods=['POST'])
def add_contact():
    if request.method == 'POST':
        nombre = request.form['nombre']
        apellido = request.form['apellido']
        correo = request.form['correo']
        contraseña = request.form['contraseña']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO usuarios(nombre,apellido,correo,contraseña) VALUES (%s, %s, %s, %s)',
        (nombre, apellido, correo, contraseña))
        mysql.connection.commit()
        flash('Registro Exitoso')
        #REENVIAMOS LA INSERCION A LA PAG PRINCIPAL
        return redirect(url_for('Index'))



if __name__ == '__main__':
    app.run(port = 3000, debug = True)
